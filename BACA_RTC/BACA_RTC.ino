#include <DS3231.h>
#include <Wire.h>

DS3231 clock1;
bool century = 0;
bool h12Flag;
bool pmFlag;
static int second = 1676796220;
void setup() {
    // Start the serial port
    Serial.begin(9600);
    // Start the I2C interface
    Wire.begin();
        clock1.setClockMode(1);  // set to 24h
        clock1.setEpoch(1676796220,1);
}

void loop() {

  Serial.print(clock1.getYear(), DEC);
  Serial.print('/');
  // then the month
  Serial.print(clock1.getMonth(century), DEC);
  Serial.print("/");
  // then the date
  Serial.print(clock1.getDate(), DEC);
  Serial.print(" ");
  // Finally the hour, minute, and second
  Serial.print(7+clock1.getHour(h12Flag, pmFlag), DEC);
  Serial.print(":");
  Serial.print(clock1.getMinute(), DEC);
  Serial.print(":");
  Serial.print(clock1.getSecond(), DEC);
  // Display the temperature
  Serial.print("T=");
  Serial.print(clock1.getTemperature(), 2);
  Serial.println();
    delay(1000);
}
