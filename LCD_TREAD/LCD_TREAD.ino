#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
#include <WiFiClient.h>
#include <LiquidCrystal_I2C.h>
#include <DS3231.h>
#include <Wire.h>
#include <TaskScheduler.h>

int check_esd = 1;
bool status_ESD = 0;
//#define ESD_state(adc) if(adc >= 100) answer = 1
////bool answer = 0; if (adc >= 100)answer = 1;

#define ESD_state(esd)                                     \
  ({                                                         \
    bool answer;                                           \
    if ((esd) >= 100) {                                    \
      answer = true;                                     \
    } else {                                               \
      answer = false;                                    \
    }                                                      \
    answer;                                                \
  })


bool esd_state, rfid_state;

DS3231 clock1;
bool century = false;

bool h12Flag;
bool pmFlag;

#define RST_PIN 2 // D4
#define SDA_PIN 15 // D8

const int buzzerPin = 16; // D0
LiquidCrystal_I2C lcd(0x27, 20, 4);

void taskCheckWrist();
void taskDisplay();
int hour_t = 0;

//Wifi Config
const char* ssid     = "B28-Lantai 2";
const char* password = "W68L2789";

Scheduler thread;
Task t1 (1 * TASK_SECOND, TASK_FOREVER, &taskCheckWrist, &thread, true);
Task t2 (1 * TASK_SECOND, TASK_FOREVER, &taskDisplay, &thread, true);


//Server Address
const char* serverTime    = "http://192.168.31.145/dev/time.php";
const char* serverInsert  = "http://192.168.31.145/dev/esp-post-insert-data.php";   // perubahan esd pas dinyalakan check waktu tampilan awal failed
const char* serverUpdate  = "http://192.168.31.145/dev/esp-post-update-data.php";
const char* serverLogIns  = "http://192.168.31.145/dev/esp-post-logins-data.php";
const char* serverLogUpt  = "http://192.168.31.145/dev/esp-post-logupt-data.php";

String apiKeyValue = "tPmAT5Ab3j7F9";
String shiftDisplay = "";
String IDDevice = "";

bool connCheck = false;

int x = 0;
int timeActual = 0;
int EsdStatus = 0;
int prevStatus = 0;
int esdLock1Status = 0;
int esdLock2Status = 0;
unsigned long esdDelay = 3000;
unsigned long esdTimePoint = 0;

void setup() {
  Serial.begin(115200);
  Wire.begin();
  clock1.setClockMode(false);  // set to 24h
  lcd.init();
  lcd.backlight();
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Connecting");
  lcd.setCursor(0, 1);

  pinMode(buzzerPin, OUTPUT);
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    if (x > 20) {
      lcd.setCursor(0, 1);
      lcd.print("                    ");
      lcd.setCursor(0, 1);
      x = 0;
    }
    delay(100);
    lcd.print(".");
    x++;
  }
  x = 0;
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Connected");
  Serial.print("Connected\r\n");
  lcd.setCursor(0, 1);
  lcd.print(WiFi.localIP());
  Serial.print(WiFi.localIP());

  IPAddress IPDevice = WiFi.localIP();
  IDDevice = String(IPDevice[3]);

  WiFi.setAutoReconnect(true);
  WiFi.persistent(true);

  delay(2000);

  //  checkTime();
  delay(2000);
}

void loop() {
  thread.execute();
}




void taskDisplay()
{

  hour_t = 7 + clock1.getHour(h12Flag, pmFlag);
  Serial.print(clock1.getYear(), DEC);
  Serial.print('/');

  // then the month
  Serial.print(clock1.getMonth(century), DEC);
  Serial.print("/");

  // then the date
  Serial.print(clock1.getDate(), DEC);
  Serial.print(" ");

  // Finally the hour, minute, and second
  Serial.print(7 + clock1.getHour(h12Flag, pmFlag), DEC);
  Serial.print(":");
  Serial.print(clock1.getMinute(), DEC);
  Serial.print(":");
  Serial.print(clock1.getSecond(), DEC);

  // Display the temperature
  Serial.print("T=");
  Serial.print(clock1.getTemperature(), 2);
  Serial.println();

  lcd.setCursor(0, 0);
  lcd.print("     ");
  lcd.setCursor(0, 0);
  lcd.print(shiftDisplay);
  lcd.setCursor(17, 0);
  lcd.print("   ");
  lcd.setCursor(17, 0);
  lcd.print(IDDevice);
  if (esd_state)
  {
    if (!status_ESD)
    {
      LCD_CHECK_ESD(check_esd);
      check_esd++;
    }
    else
    {
      if (!rfid_state) printMenu2();
      else             printMenu2(); 
    }
    if (check_esd == 5)
    {
      check_esd = 0;
      status_ESD = 1;
    }
  }
  else { // ESD NG & RFID NG
    LCD_DISC_ESD_RFID();
    status_ESD = check_esd = 0;
  }
  if (rfid_state)                Serial.println("RFID : CONNECTED");
  else                           Serial.println("RFID : NOT CONNECTED");
  if (esd_state)                 Serial.println("ESC  : CONNECTED");
  else                           Serial.println("ESC  : NOT CONNECTED");
  if (status_ESD)                Serial.println("ESC_status  : CONNECTED");
  else                           Serial.println("ESC_status  : NOT CONNECTED");
}

void taskCheckWrist()
{
  esd_state = ESD_state(analogRead(A0));
}



void LCD_DISC_ESD_RFID() {
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print(shiftDisplay);
  lcd.setCursor(7, 0);
  lcd.print("ESD NG");
  lcd.setCursor(17, 0);
  lcd.print(IDDevice);
  lcd.setCursor(0, 1);
  lcd.print("--------------------");
  lcd.setCursor(2, 2);
  lcd.print("SILAHKAN PASANG");
  lcd.setCursor(2, 3);
  lcd.print("ESD WRIST STRAP");
}

void LCD_CHECK_ESD(int counter) // nunggu 5 detik bos
{
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print(shiftDisplay);
  lcd.setCursor(7, 0);
  lcd.print("ESD []");
  lcd.setCursor(17, 0);
  lcd.print(IDDevice);
  lcd.setCursor(0, 1);
  lcd.print("--------------------");
  lcd.setCursor(4, 2);
  lcd.print("MENGUKUR ESD");
  lcd.setCursor(8, 3);
  lcd.print(counter);
}

void printMenu2() {
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print(shiftDisplay);
  lcd.setCursor(7, 0);
  lcd.print("ESD OK");
  lcd.setCursor(17, 0);
  lcd.print(IDDevice);
  lcd.setCursor(0, 1);
  lcd.print("--------------------");
  lcd.setCursor(2, 2);
  lcd.print("SILAHKAN TAP ID");
  lcd.setCursor(3, 3);
  lcd.print("CARD KARYAWAN");
}

void checkTime() {  // tiap menit dipakai access php di
  String responsePayload = "";
  connCheck = true;
  while (WiFi.status() != WL_CONNECTED) {
    if (connCheck)
    {
      lcd.clear();
      lcd.setCursor(0, 0);
      lcd.print("Connecting");
      lcd.setCursor(0, 1);
      connCheck = false;
    }
    if (x > 20)
    {
      lcd.setCursor(0, 1);
      lcd.print("                    ");
      lcd.setCursor(0, 1);
      x = 0;
    }
    delay(100);
    lcd.print(".");
    x++;
  }
  x = 0;
  connCheck = false;

  IPAddress IPDevice = WiFi.localIP();
  IDDevice = String(IPDevice[3]);

  WiFiClient client;
  HTTPClient http;

  http.begin(client, serverTime);
  http.addHeader("Content-Type", "application/x-www-form-urlencoded");

  String httpRequestData = "api_key=" + apiKeyValue + "";
  Serial.print("httpRequestData: ");
  Serial.println(httpRequestData);

  int httpResponseCode = http.POST(httpRequestData);

  if (httpResponseCode > 0) {
    const String& payload = http.getString();
    responsePayload = String(payload);
    timeActual = responsePayload.toInt();  // access php
    Serial.println(responsePayload);
    Serial.print("HTTP Response code: ");
    Serial.println(httpResponseCode);
    Serial.print(timeActual);
    clock1.setEpoch(timeActual, 1);
  }
  else {
    Serial.print("Error code: ");
    Serial.println(httpResponseCode);
  }
  http.end();
}
