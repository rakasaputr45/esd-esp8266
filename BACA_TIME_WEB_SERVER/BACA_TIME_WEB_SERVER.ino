#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
#include <WiFiClient.h>
#include <Wire.h> 
#include <LiquidCrystal_I2C.h>
#include <SPI.h>
#include <MFRC522.h>
#include <time.h>
#define RST_PIN 2 // D4
#define SDA_PIN 15 // D8

const int buzzerPin = 16; // D0
void printLocalTime();
MFRC522 mfrc522(SDA_PIN, RST_PIN);
LiquidCrystal_I2C lcd(0x27,20,4);

//Wifi Config
const char* ssid     = "B28-Lantai 2";
const char* password = "W68L2789";

//Server Address

const char* serverTime    = "http://192.168.31.145/dev/time.php";
const char* serverInsert  = "http://192.168.31.145/dev/esp-post-insert-data.php";   // perubahan esd pas dinyalakan check waktu tampilan awal failed 
const char* serverUpdate  = "http://192.168.31.145/dev/esp-post-update-data.php";
const char* serverLogIns  = "http://192.168.31.145/dev/esp-post-logins-data.php";
const char* serverLogUpt  = "http://192.168.31.145/dev/esp-post-logupt-data.php";

String apiKeyValue = "tPmAT5Ab3j7F9";
String shiftDisplay = "";
String IDDevice = "";

bool connCheck = false;

int x = 0;
int timeActual = 0;
int EsdStatus = 0;
int prevStatus = 0;
int esdLock1Status = 0;
int esdLock2Status = 0;
unsigned long esdDelay = 3000;
unsigned long esdTimePoint = 0;

void setup() {
  Serial.begin(115200);

  SPI.begin();
  mfrc522.PCD_Init();

  lcd.init();
  lcd.backlight();
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Connecting");
  lcd.setCursor(0, 1);

  pinMode(buzzerPin, OUTPUT);
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  while(WiFi.status() != WL_CONNECTED) { 
    if(x > 20){
      lcd.setCursor(0, 1);
      lcd.print("                    ");
      lcd.setCursor(0, 1);
      x=0;
    }
    delay(100);
    lcd.print(".");
    x++;
  }
  x = 0;
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Connected");
  Serial.print("Connected\r\n");
  lcd.setCursor(0, 1);
  lcd.print(WiFi.localIP());
  Serial.print(WiFi.localIP());

  IPAddress IPDevice = WiFi.localIP();
  IDDevice = String(IPDevice[3]);

  WiFi.setAutoReconnect(true);
  WiFi.persistent(true);

  delay(2000);

  checkTime();
  delay(2000);
}

void loop() {
  if((analogRead(A0) < 100) && (EsdStatus == 0) && (prevStatus == 0))
  {
    Serial.println("12");
    printMenu1();
    esdLock1Status = 0;
    prevStatus = 1;
    return;
  }
  if((analogRead(A0) >= 100) && (EsdStatus == 0)){
     Serial.println("22");
    if((millis() - esdTimePoint > esdDelay) && (esdLock1Status == 1))
    {
      Serial.println("3");
      EsdStatus = 1;
      esdLock1Status = 0;
      printMenu2();
    }
    if(esdLock1Status == 0){
      Serial.println("4");
      printMenuA1();
      esdLock1Status = 1;
      esdTimePoint = millis();
    }
    prevStatus = 0;
    return;
  }
}

void printMenu1(){
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print(shiftDisplay);
  lcd.setCursor(7, 0);
  lcd.print("ESD NG");
  lcd.setCursor(17, 0);
  lcd.print(IDDevice);
  lcd.setCursor(0, 1);
  lcd.print("--------------------");
  lcd.setCursor(2, 2);
  lcd.print("SILAHKAN PASANG");
  lcd.setCursor(2, 3);
  lcd.print("ESD WRIST STRAP");
}

void printMenuA1(){
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print(shiftDisplay);
  lcd.setCursor(7, 0);
  lcd.print("ESD []");
  lcd.setCursor(17, 0);
  lcd.print(IDDevice);
  lcd.setCursor(0, 1);
  lcd.print("--------------------");
  lcd.setCursor(4, 2);
  lcd.print("MENGUKUR ESD");
  lcd.setCursor(8, 3);
  lcd.print("....");
}

void printMenu2(){
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print(shiftDisplay);
  lcd.setCursor(7, 0);
  lcd.print("ESD OK");
  lcd.setCursor(17, 0);
  lcd.print(IDDevice);
  lcd.setCursor(0, 1);
  lcd.print("--------------------");
  lcd.setCursor(2, 2);
  lcd.print("SILAHKAN TAP ID");
  lcd.setCursor(3, 3);
  lcd.print("CARD KARYAWAN");
}

void checkTime(){   // tiap menit dipakai access php di
  String responsePayload = "";
  connCheck = true;
  while(WiFi.status() != WL_CONNECTED) {
    if(connCheck){
      lcd.clear();
      lcd.setCursor(0, 0);
      lcd.print("Connecting");
      lcd.setCursor(0, 1);
      connCheck = false;
    }

    if(x > 20){
      lcd.setCursor(0, 1);
      lcd.print("                    ");
      lcd.setCursor(0, 1);
      x=0;
    }
    delay(100);
    lcd.print(".");
    x++;
  }
  x = 0;
  connCheck = false;

  IPAddress IPDevice = WiFi.localIP();
  IDDevice = String(IPDevice[3]);

  WiFiClient client;
  HTTPClient http;

  http.begin(client, serverTime);
  http.addHeader("Content-Type", "application/x-www-form-urlencoded");

  String httpRequestData = "api_key=" + apiKeyValue + "";
  Serial.print("httpRequestData: ");
  Serial.println(httpRequestData);
  
  int httpResponseCode = http.POST(httpRequestData);
      
  if (httpResponseCode>0) {
    const String& payload = http.getString();
    responsePayload = String(payload);
    timeActual = responsePayload.toInt();  // access php 
    Serial.println(responsePayload);        
    Serial.print("HTTP Response code: ");
    Serial.println(httpResponseCode);
    Serial.print(timeActual);
  }
  else {
    Serial.print("Error code: ");
    Serial.println(httpResponseCode);
  }
  http.end();
  
  if(timeActual == 6 || timeActual == 7)
  {
    shiftDisplay = "IN-1";
  }
  else if(timeActual == 16 || timeActual == 17)
  {
    shiftDisplay = "OUT-1";
  }
  else{
    shiftDisplay = "OFF";
  }

  lcd.setCursor(0, 0);
  lcd.print("     ");
  lcd.setCursor(0, 0);
  lcd.print(shiftDisplay);
  lcd.setCursor(17, 0);
  lcd.print("   ");
  lcd.setCursor(17, 0);
  lcd.print(IDDevice);
}
